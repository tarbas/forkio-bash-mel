// $('#prev').click(function () {
//     let currentIndex = $('.active').index();
//     $(".social__feedback-carousel-item").eq(currentIndex).fadeOut(500);
//     $('.social__feedback-carousel-item').removeClass('active');
//     $('.social__feedback-carousel-item').eq(currentIndex - 1).addClass('active');
//     $(".social__feedback-carousel-item").eq(currentIndex - 1).fadeIn(500);
// });

// $('#next').click(function () {
//     let currentIndex = $('.active').index();

//     $(".social__feedback-carousel-item").eq(currentIndex).fadeOut(500);

//     currentIndex = currentIndex === $('.social__feedback-carousel-item').length - 1 ?
//         -1 : $('.social__feedback-carousel-item').index();
//     $('.social__feedback-carousel-item').removeClass('active');
//     $('.social__feedback-carousel-item').eq(currentIndex + 1).addClass('active');
//     $(".social__feedback-carousel-item").eq(currentIndex + 1).fadeIn(500);
// });

// Идея слайдера от Славы

var slider = [];
slider.add = function (url, name, nickname, text, time) {
    let element = document.createElement('div');
    element.className = "social__feedback-carousel-item";

    let authorDiv = document.createElement('div');
    authorDiv.className = "social__feedback-author";


    let userPic = document.createElement('div');
    userPic.className = "social__feedback-author-userpic";
    userPic.style.backgroundImage = `url("${url}")`;

    let captionWrapper = document.createElement('div');
    captionWrapper.className = "social__feedback-author-caption-wrapper";

    let nameEl = document.createElement('div');
    nameEl.className = "social__feedback-author-caption-name";
    nameEl.innerText = name;

    let nicknameEl = document.createElement('div');
    nicknameEl.className = "social__feedback-author-caption-nickname";
    nicknameEl.innerText = nickname;

    let textEl = document.createElement('div');
    textEl.className = "social__feedback-carousel-item-text";
    textEl.innerHTML = text;

    let timeEl = document.createElement('div');
    timeEl.className = "social__feedback-carousel-item-time";
    timeEl.innerText = time;

    captionWrapper.appendChild(nameEl);
    captionWrapper.appendChild(nicknameEl);

    authorDiv.appendChild(userPic);
    authorDiv.appendChild(captionWrapper);

    element.appendChild(authorDiv);
    element.appendChild(textEl);
    element.appendChild(timeEl);

    this.push(element);
};

slider.currentIndex = 0;

slider.add('./img/320/userpic-carousel.png',
'Steven Strange',
'@DoctorS',
' <span>@Pixelbuddha</span> Suspendisse sodales sem est_ in scelerisque felis. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptates nam eius nihil.',
'3 hours ago');

slider.add('./img/320/john.jpg',
'John Snow',
'Gogi',
' <span>@JsGod</span> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptates nam eius nihil.',
'5 hours ago');

slider.add('./img/320/cardi.jpeg',
'Cardi G',
'Gina',
' <span>@JsGod</span> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi, ullam recusandae.',
'4 hours ago');


slider.next = function () {
    if (++this.currentIndex > this.length - 1) this.currentIndex = 0;
    return this[this.currentIndex];
};

slider.prev = function () {
    if (--this.currentIndex < 0) this.currentIndex = this.length - 1;
    let index = this.currentIndex;
    return this[index];
};

document.getElementById('prev').onclick = function (evt) {
    evt.preventDefault();
    let wrap = document.getElementById('wrap');

    wrap.innerHTML = "";
    wrap.appendChild(slider.prev());
}

document.getElementById('next').onclick = function (evt) {
    evt.preventDefault();
    let wrap = document.getElementById('wrap');

    wrap.innerHTML = "";

    wrap.appendChild(slider.next());
}

//замена SUBSCRIBE на CALL ME!

function changeSubscribeBtn () {
    if ($('body').outerWidth() > 768) {
        $('#subscribe-call-btn').val("CALL ME!");
    } else { $('#subscribe-call-btn').val("SUBSCRIBE");};
};

changeSubscribeBtn();

$(window).resize(changeSubscribeBtn);
