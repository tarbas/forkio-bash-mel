const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const uglify = require('gulp-uglify');
const pump = require('pump');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const clean = require('gulp-clean');
const gulpSequence = require('gulp-sequence');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');

//clean task

gulp.task('clean-dist', function(){
    return gulp.src('./dist', {read: false})
        .pipe(clean());
});

// HTML task

gulp.task('copy-html', function(){
    return gulp.src('./src/**/*.html')
               .pipe(gulp.dest('./dist'));
});

// CSS task 


gulp.task('concat-scss', function() {
    return gulp.src(['./src/**/*.scss','!./src/scss/all.scss'])
           .pipe(concat('all.scss'))
           .pipe(gulp.dest('./src/scss'));
   });

gulp.task('sass', function () {
  return gulp.src('./src/scss/all.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./src/css'));
});

gulp.task('autoprefixer', () =>
    gulp.src('src/**/all.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./src'))
);

gulp.task('minify-css', () => {
  return gulp.src('src/css/all.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('css', function (cb) {gulpSequence('concat-scss', 'sass', 'autoprefixer', 'minify-css')(cb)});

//JS task

gulp.task('concat-js', function() {
    return gulp.src(['./src/js/**/*.js','!./src/js/all.js', '!./src/js/all.min.js', '!./src/js/jquery-3.3.1.min.js'])
           .pipe(concat('all.js'))
           .pipe(gulp.dest('./src/js/'));
   });

gulp.task('transpile', ['concat-js'], () =>
    gulp.src('./src/js/all.js')
        .pipe(babel({presets: ["@babel/env"]}))
        .pipe(gulp.dest('./src/js/'))
);

gulp.task('compress', ['transpile'], function (cb) {
    pump([
            gulp.src('./src/js/all.js'),
            uglify(),
            rename("/all.min.js"),
            gulp.dest('src/js/')
        ],
        cb
    );
});

gulp.task('copy-js', ['compress'], function(){
    return gulp.src(['./src/js/all.min.js','./src/js/jquery-3.3.1.min.js'])
        .pipe(gulp.dest ('./dist/js/'));
});



//imagemin

gulp.task('imagemin', () =>
    gulp.src('src/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img/'))
);


gulp.task('dev', function(){
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch('./src/**/*.html',['copy-html']).on('change',browserSync.reload);
    gulp.watch('./src/scss/**/*.scss',['css']).on('change',browserSync.reload);
    gulp.watch('./src/js/**/*.js',['copy-js']).on('change',browserSync.reload);

});

gulp.task('build', gulpSequence('clean-dist', 'css', ['copy-html','copy-js'], 'imagemin'));